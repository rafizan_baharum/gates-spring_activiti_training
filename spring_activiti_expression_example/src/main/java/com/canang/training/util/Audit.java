package com.canang.training.util;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Listneer style of calling java
 *
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("audit")
public class Audit {

    private static final Logger LOG = Logger.getLogger(Audit.class);

    public void log() {
        LOG.debug("auditing");
    }
}
