package com.canang.training.util;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Listneer style of calling java
 *
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("startAudit")
public class StartAudit {

    private static final Logger LOG = Logger.getLogger(StartAudit.class);

    public void log() {
        LOG.debug("start auditing");
    }
}
