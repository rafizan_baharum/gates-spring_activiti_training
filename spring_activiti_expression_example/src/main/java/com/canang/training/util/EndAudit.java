package com.canang.training.util;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Listneer style of calling java
 *
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("endAudit")
public class EndAudit {

    private static final Logger LOG = Logger.getLogger(EndAudit.class);

    public void log() {
        LOG.debug("end auditing");
    }
}
