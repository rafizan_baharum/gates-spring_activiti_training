package com.canang.training.util;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */

@Component("util")
public class Util {

    private static final Logger LOG = Logger.getLogger(Util.class);

    public LocalDate getDateTime() throws Exception {
        return new LocalDate();
    }
}
