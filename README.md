Activiti BPM Workflow training
===============================

This training covers the basics of using Activiti BPM.
Activiti is a light-weight workflow and Business Process Management (BPM) Platform targeted at business people, developers and system admins.


Prerequisites
-------------
- Good understanding of Java
- Understand how Spring framework works
- [Activiti] setup
- Database ([H2 Database][H2])
- Understand existing application workflow
- Maven based project
- Git ([Bitbucket])
- Some Java IDE (Eclipse recommended)


Approach
--------
This is a hands-on approach that uses JUnit tests to solve the exercise.


[H2]:http://www.h2database.com/
[Bitbucket]:http://www.bitbucket.com/
[Activiti]:http://www.activiti.org/
