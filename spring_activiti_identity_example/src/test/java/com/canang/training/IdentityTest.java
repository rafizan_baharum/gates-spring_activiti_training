package com.canang.training;

import com.canang.training.config.DataSourceConfiguration;
import com.canang.training.config.EntityConfiguration;
import com.canang.training.config.WorkflowConfiguration;
import com.canang.training.dao.GroupDao;
import com.canang.training.dao.UserDao;
import com.canang.training.model.Group;
import com.canang.training.model.User;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author : alif haikal razak
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceConfiguration.class, WorkflowConfiguration.class, EntityConfiguration.class})
@Transactional
public class IdentityTest {

    private static final Logger LOG = Logger.getLogger(IdentityTest.class);

    private static final String PROCESS_DEFINITION_KEY = "registration_identity_workflow";
    private static final String RESOURCE_PATH = "definition/registration_identity_workflow.bpmn20.xml";
    private static final String PROCESS_NAME = "registration_identity_workflow";

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GroupDao groupDao;

    @Autowired
    private SessionFactory sessionFactory;

    @Before
    public void setUp() {

        // set user and group
        User pegawai1 = new User();
        pegawai1.setUsername("PEGAWAI1");
        pegawai1.setPassword("ABC123");

        User pegawai2 = new User();
        pegawai2.setUsername("PEGAWAI2");
        pegawai2.setPassword("ABC123");

        User kerani1 = new User();
        kerani1.setUsername("KERANI1");
        kerani1.setPassword("ABC123");

        User kerani2 = new User();
        kerani2.setUsername("KERANI2");
        kerani2.setPassword("ABC123");

        userDao.save(pegawai1);
        userDao.save(pegawai2);
        userDao.save(kerani1);
        userDao.save(kerani2);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().refresh(pegawai1);
        sessionFactory.getCurrentSession().refresh(pegawai2);
        sessionFactory.getCurrentSession().refresh(kerani1);
        sessionFactory.getCurrentSession().refresh(kerani2);

        Group groupPegawai = new Group();
        groupPegawai.setName("GROUP PEGAWAI");
        Group groupKerani = new Group();
        groupKerani.setName("GROUP KERANI");
        groupDao.save(groupPegawai);
        groupDao.save(groupKerani);

        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().refresh(groupPegawai);
        sessionFactory.getCurrentSession().refresh(groupKerani);

        groupDao.addMember(groupPegawai, pegawai1);
        groupDao.addMember(groupPegawai, pegawai2);
        groupDao.addMember(groupKerani, kerani1);
        groupDao.addMember(groupKerani, kerani2);
        sessionFactory.getCurrentSession().flush();
    }

    @After
    public void tearDown() {
    }


    @Test
    @Rollback(true)
    public void signalTask() throws Exception {
        assertEquals("Workflow Engine", processEngine.getName());
        assertNotNull(runtimeService);
        assertNotNull(taskService);
        assertNotNull(repositoryService);

        // deployment
        DeploymentBuilder deployment = repositoryService.createDeployment();

        // check if process def already exits
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> list = query.processDefinitionKey(PROCESS_NAME).list();
        LOG.debug("count: " + list.size());

        // start only when we don't have one
        if (list.isEmpty()) {
            // builder chaining
            deployment
                    .addClasspathResource(RESOURCE_PATH)
                    .name(PROCESS_NAME)
                    .deploy();


        }

        // start process with empty variable
        HashMap<String, Object> variables = new HashMap<String, Object>();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);
        assertNotNull(processInstance.getId());
        LOG.debug("Process instance created: " + processInstance.getId());

        // RECEIVE >> CALL
        TaskQuery taskQuery = taskService.createTaskQuery();
        List<Task> tasks = taskQuery.taskName("receiveTaskIdentity").list();
        assertTrue(!tasks.isEmpty());

        TaskQuery taskQuery2 = taskService.createTaskQuery();
        tasks = taskQuery2.taskCandidateGroup("GROUP KERANI").list();
        assertTrue(!tasks.isEmpty());

        TaskQuery taskQuery3 = taskService.createTaskQuery();
        tasks = taskQuery3.taskCandidateUser("KERANI1").list();
        assertTrue(!tasks.isEmpty());

        assertNotNull(tasks);
        for (Task task : tasks) {
            taskService.complete(task.getId());
        }

        // CALL >> END
        TaskQuery taskQuery4 = taskService.createTaskQuery();
        tasks = taskQuery4.taskCandidateGroup("GROUP PEGAWAI").list();
        assertTrue(!tasks.isEmpty());

        TaskQuery taskQuery5 = taskService.createTaskQuery();
        tasks = taskQuery5.taskCandidateUser("PEGAWAI1").list();
        assertTrue(!tasks.isEmpty());

        assertNotNull(tasks);
        for (Task task : tasks) {
            taskService.complete(task.getId());
        }
    }
}
