package com.canang.training.task;

import org.activiti.engine.impl.bpmn.behavior.BpmnActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("callTaskComponent")
public class CallTask extends BpmnActivityBehavior
        implements ActivityBehavior {

    private static final Logger LOG = org.apache.log4j.Logger.getLogger(CallTask.class);

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {
        LOG.debug("executing");
    }
}
