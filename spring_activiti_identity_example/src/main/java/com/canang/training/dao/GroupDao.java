package com.canang.training.dao;

import com.canang.training.model.Group;
import com.canang.training.model.GroupMember;
import com.canang.training.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@SuppressWarnings({"unchecked"})
@Repository("groupDao")
public class GroupDao {

    @Autowired
    private SessionFactory sessionFactory;

    public Group findById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return (Group) session.get(Group.class, id);
    }

    public Group findByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r from Group r where r.name = :name");
        query.setString("name", name);
        return (Group) query.uniqueResult();
    }

    public List<Group> find() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r from Group r");
        return (List<Group>) query.list();
    }

    public List<User> findMembers(Group group) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r.user from GroupMember r where r.group = :group");
        query.setEntity("group", group);
        return (List<User>) query.list();
    }

    public List<Group> findMemberships(User user) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r.group from GroupMember r where r.user = :user");
        query.setEntity("user", user);
        return (List<Group>) query.list();
    }

    public void save(Group group) {
        Session session = sessionFactory.getCurrentSession();
        session.save(group);
    }

    public void addMember(Group group, User user) {
        GroupMember groupMember = new GroupMember();
        groupMember.setUser(user);
        groupMember.setGroup(group);
        Session session = sessionFactory.getCurrentSession();
        session.save(groupMember);
    }

}
