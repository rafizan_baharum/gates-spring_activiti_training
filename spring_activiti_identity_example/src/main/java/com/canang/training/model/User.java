package com.canang.training.model;

import javax.persistence.*;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Entity(name = "User")
@Table(name = "USER")
public class User {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_USER")
    @SequenceGenerator(name = "SEQ_USER", sequenceName = "SEQ_USER", allocationSize = 1)
    private Long id;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
