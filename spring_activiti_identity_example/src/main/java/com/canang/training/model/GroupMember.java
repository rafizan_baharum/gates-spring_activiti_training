package com.canang.training.model;

import javax.persistence.*;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */

@Entity(name = "GroupMember")
@Table(name = "GROUP_MEMBER")
public class GroupMember {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_GROUP_MEMBER")
    @SequenceGenerator(name = "SEQ_GROUP_MEMBER", sequenceName = "SEQ_GROUP_MEMBER", allocationSize = 1)
    private Long id;

    @OneToOne(targetEntity = Group.class)
    @JoinColumn(name = "GROUP_ID")
    private Group group;

    @OneToOne(targetEntity = User.class)
    @JoinColumn(name = "USER_ID")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
