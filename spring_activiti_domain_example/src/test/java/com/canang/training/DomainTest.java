package com.canang.training;

import com.canang.training.config.DataSourceConfiguration;
import com.canang.training.config.EntityConfiguration;
import com.canang.training.config.WorkflowConfiguration;
import com.canang.training.dao.RegistrationDao;
import com.canang.training.model.Registration;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author : alif haikal razak
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceConfiguration.class, WorkflowConfiguration.class, EntityConfiguration.class})
@Transactional
public class DomainTest {

    private static final Logger LOG = Logger.getLogger(DomainTest.class);

    private static final String PROCESS_DEFINITION_KEY = "registration_domain_workflow";
    private static final String RESOURCE_PATH = "definition/registration_domain_workflow.bpmn20.xml";
    private static final String PROCESS_NAME = "registration_domain_workflow";
    public static final String REGISTRATION_ID = "registrationId";

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private RegistrationDao registrationDao;

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    @Rollback(true)
    public void signalTask() throws Exception {
        assertEquals("Workflow Engine", processEngine.getName());
        assertNotNull(runtimeService);
        assertNotNull(taskService);
        assertNotNull(repositoryService);

        // deployment
        DeploymentBuilder deployment = repositoryService.createDeployment();

        // check if process def already exits
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> list = query.processDefinitionKey(PROCESS_NAME).list();
        LOG.debug("count: " + list.size());

        // start only when we don't have one
        if (list.isEmpty()) {
            // builder chaining
            deployment
                    .addClasspathResource(RESOURCE_PATH)
                    .name(PROCESS_NAME)
                    .deploy();


        }

        // start process with registration variable
        Registration registration = new Registration();
        registration.setCourseName("ENGINEERING101");
        registration.setStudentName("Rafizan Baharum");
        registrationDao.save(registration);
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().refresh(registration);
        LOG.debug("registration: " + registration.getId());

        HashMap<String, Object> variables = new HashMap<String, Object>();
        variables.put(REGISTRATION_ID, registration.getId());
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);
        assertNotNull(processInstance.getId());
        LOG.debug("Process instance created: " + processInstance.getId());

        // RECEIVE >> CALL
        TaskQuery taskQuery = taskService.createTaskQuery();
        List<Task> tasks = taskQuery.taskName("receiveTaskDomain").list();
        LOG.debug("found tasks size: " + tasks.size());

        assertNotNull(tasks);
        for (Task task : tasks) {
            Long regId = (Long) runtimeService.getVariable(task.getExecutionId(), REGISTRATION_ID);
            assertEquals(registration.getId(),regId);
            LOG.debug("registration: " + regId);
            taskService.complete(task.getId());
        }

        // CALL >> END
        taskQuery = taskService.createTaskQuery();
        tasks = taskQuery.taskName("callTaskDomain").list();
        LOG.debug("found tasks size: " + tasks.size());

        assertNotNull(tasks);
        for (Task task : tasks) {
            Long regId = (Long) runtimeService.getVariable(task.getExecutionId(), REGISTRATION_ID);
            assertEquals(registration.getId(),regId);
            LOG.debug("registration: " + regId);
            taskService.complete(task.getId());
        }
    }
}
