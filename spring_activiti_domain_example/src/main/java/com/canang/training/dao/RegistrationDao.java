package com.canang.training.dao;

import com.canang.training.model.Registration;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Repository("registrationDao")
public class RegistrationDao {

    @Autowired
    private SessionFactory sessionFactory;

    public Registration findById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return (Registration) session.get(Registration.class, id);
    }

    public List<Registration> find() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r from Registration r");
        return (List<Registration>) query.list();
    }

    public void save(Registration registration) {
        Session session = sessionFactory.getCurrentSession();
        session.save(registration);
    }
}
