package com.canang.training.model;

import javax.persistence.*;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Entity(name = "Registration")
@Table(name = "REGISTRATION")
public class Registration {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_REGISTRATION")
    @SequenceGenerator(name = "SEQ_REGISTRATION", sequenceName = "SEQ_REGISTRATION", allocationSize = 1)
    private Long id;

    @Column(name = "STUDENT_NAME")
    private String studentName;

    @Column(name = "COURSE_NAME")
    private String courseName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
