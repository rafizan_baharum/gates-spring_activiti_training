package com.canang.training.util;

import com.canang.training.dao.RegistrationDao;
import com.canang.training.model.Registration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("advanceAudit")
public class AdvanceAudit {

    private static final Logger LOG = Logger.getLogger(AdvanceAudit.class);

    @Autowired
    private RegistrationDao registrationDao;

    public void log(Long registrationId) {
        LOG.debug("auditing");
        Registration registration = registrationDao.findById(registrationId);
        LOG.debug("auditing student registration : " + registration.getStudentName());
    }
}
