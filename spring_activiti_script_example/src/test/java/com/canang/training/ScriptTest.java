package com.canang.training;

import com.canang.training.config.DataSourceConfiguration;
import com.canang.training.config.WorkflowConfiguration;
import junit.framework.Assert;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.history.HistoricVariableInstanceQuery;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author : alif haikal razak
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceConfiguration.class, WorkflowConfiguration.class})
@Transactional
public class ScriptTest {

    private static final Logger LOG = Logger.getLogger(ScriptTest.class);

    private static final String PROCESS_DEFINITION_KEY = "registration_expression_workflow";
    private static final String RESOURCE_PATH = "definition/registration_expression_workflow.bpmn20.xml";
    private static final String PROCESS_NAME = "registration_expression_workflow";

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private HistoryService historyService;

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    @Rollback(true)
    public void scriptTest() throws Exception {
        assertEquals("Workflow Engine", processEngine.getName());
        assertNotNull(runtimeService);
        assertNotNull(taskService);
        assertNotNull(repositoryService);

        // deployment
        DeploymentBuilder deployment = repositoryService.createDeployment();

        // check if process def already exits
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> list = query.processDefinitionKey(PROCESS_NAME).list();
        LOG.debug("count: " + list.size());

        // start only when we don't have one
        if (list.isEmpty()) {
            // builder chaining
            deployment
                    .addClasspathResource(RESOURCE_PATH)
                    .name(PROCESS_NAME)
                    .deploy();
        }

        // set your variables here
        HashMap<String, Object> variables = new HashMap<String, Object>();
        variables.put("username", "Mike Wazowski");
        variables.put("paidAmount", 60);
        variables.put("feeAmount", 50);

        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);
        assertNotNull(processInstance.getId());
        LOG.debug("Process instance created: " + processInstance.getId());

        HistoricVariableInstance instance = historyService.createHistoricVariableInstanceQuery()
                .processInstanceId(processInstance.getId()).variableName("proceed").singleResult();
        Assert.assertTrue("Value must be true in order to proceed", (Boolean) instance.getValue());
        LOG.debug("Need to be reviewed = " + instance.getValue());

    }
}
