package com.canang.training.task;

import org.activiti.engine.impl.bpmn.behavior.BpmnActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */

@Component("reviewTaskComponent")
public class ReviewTask extends BpmnActivityBehavior
        implements ActivityBehavior {

    private static final Logger LOG = Logger.getLogger(ReviewTask.class);

    @Override
    public void execute(ActivityExecution activityExecution) throws Exception {
        LOG.debug("executing review task");
        Boolean proceed = (Boolean) activityExecution.getVariable("proceed");
        activityExecution.setVariable("proceed", proceed);
    }
}
