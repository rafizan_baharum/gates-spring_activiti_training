package com.canang.training;

import com.canang.training.config.DataSourceConfiguration;
import com.canang.training.config.WorkflowConfiguration;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author : alif haikal razak
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceConfiguration.class, WorkflowConfiguration.class})
@Transactional
public class DeploymentTest {

    private static final Logger LOG = Logger.getLogger(DeploymentTest.class);

    private static final String PROCESS_DEFINITION_KEY = "registration_workflow";
    private static final String RESOURCE_PATH = "definition/registration_workflow.bpmn20.xml";
    private static final String PROCESS_NAME = "registration_workflow";

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RepositoryService repositoryService;

    @Test
    @Rollback(false)
    public void startProcess() throws Exception {
        assertEquals("Workflow Engine", processEngine.getName());
        assertNotNull(runtimeService);
        assertNotNull(taskService);
        assertNotNull(repositoryService);

        // deployment
        DeploymentBuilder deployment = repositoryService.createDeployment();

        // check if process def already exits
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> list = query.processDefinitionKey(PROCESS_NAME).list();
        LOG.debug("count: " + list.size());

        // start only when we don't have one
        if (list.isEmpty()) {
            // builder chaining
            deployment
                    .addClasspathResource(RESOURCE_PATH)
                    .name(PROCESS_NAME)
                    .deploy();

        }

        // start process with empty variable
        HashMap<String, Object> variables = new HashMap<String, Object>();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);
        assertNotNull(processInstance.getId());


        // untuk listing page
        LOG.debug("Process instance created: " + processInstance.getId());
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery.taskName("receiveTask");
        taskQuery.taskCandidateUser("KERANI"); // candidate
        taskQuery.taskAssignee("AHMAD");  // owner
        List<Task> list1 = taskQuery.list();// list
        List<Task> tasks = taskQuery.taskName("receiveTask").list();


         // view page + link button
        Task task = taskQuery.singleResult();// single
        LOG.debug("tasks size: " + tasks.size());
        assertTrue(!tasks.isEmpty());
        taskService.complete(task.getId());           //
    }
}
