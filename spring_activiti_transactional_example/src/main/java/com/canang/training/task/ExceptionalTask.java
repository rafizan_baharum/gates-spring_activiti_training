package com.canang.training.task;

import org.activiti.engine.impl.bpmn.behavior.BpmnActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.springframework.stereotype.Component;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */

@Component("exceptionalTask")
public class ExceptionalTask extends BpmnActivityBehavior implements ActivityBehavior {
    @Override
    public void execute(ActivityExecution execution) throws Exception {
        if (execution.hasVariable("setError") &&
                Boolean.valueOf(execution.getVariable("setError").toString())) {
            throw new UnsupportedOperationException("Rollback activiti!!");
        }
    }
}

