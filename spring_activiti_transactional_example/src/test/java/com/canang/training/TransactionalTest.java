package com.canang.training;

import com.canang.training.config.DataSourceConfiguration;
import com.canang.training.config.WorkflowConfiguration;
import org.activiti.engine.*;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author : alif haikal razak
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceConfiguration.class, WorkflowConfiguration.class})
@Transactional
public class TransactionalTest {

    private static final Logger LOG = Logger.getLogger(TransactionalTest.class);

    private static final String PROCESS_DEFINITION_KEY = "transactional_workflow";
    private static final String RESOURCE_PATH = "definition/transactional_workflow.bpmn20.xml";
    private static final String PROCESS_NAME = "transactional_workflow";

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private HistoryService historyService;

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    @Test
    @Rollback(true)
    public void rollbackTransaction() throws Exception {
        assertEquals("Workflow Engine", processEngine.getName());
        assertNotNull(runtimeService);
        assertNotNull(taskService);
        assertNotNull(repositoryService);

        // deployment
        DeploymentBuilder deployment = repositoryService.createDeployment();

        // check if process def already exits
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> list = query.processDefinitionKey(PROCESS_NAME).list();
        LOG.debug("count: " + list.size());

        // start only when we don't have one
        if (list.isEmpty()) {
            // builder chaining
            deployment
                    .addClasspathResource(RESOURCE_PATH)
                    .name(PROCESS_NAME)
                    .deploy();
        }

        try {
            // start process with empty variable
            HashMap<String, Object> variables = new HashMap<String, Object>();
            variables.put("setError", true);

            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);
            assertNotNull(processInstance.getId());
            LOG.debug("Process instance created: " + processInstance.getId());
        } catch (Exception e) {
        }

        int numOfHistoryPI = historyService.createHistoricProcessInstanceQuery().list().size();
        assertEquals(0, numOfHistoryPI);
        System.out.println(("Number of Process Instance history = " + numOfHistoryPI));

    }

    @Test
    @Rollback(true)
    public void doNotRollBackTransaction() throws Exception {
        assertEquals("Workflow Engine", processEngine.getName());
        assertNotNull(runtimeService);
        assertNotNull(taskService);
        assertNotNull(repositoryService);

        // deployment
        DeploymentBuilder deployment = repositoryService.createDeployment();

        // check if process def already exits
        ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> list = query.processDefinitionKey(PROCESS_NAME).list();
        LOG.debug("count: " + list.size());

        // start only when we don't have one
        if (list.isEmpty()) {
            // builder chaining
            deployment
                    .addClasspathResource(RESOURCE_PATH)
                    .name(PROCESS_NAME)
                    .deploy();
        }

        try {
            // start process with empty variable
            HashMap<String, Object> variables = new HashMap<String, Object>();
            variables.put("setError", false);

            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, variables);
            assertNotNull(processInstance.getId());
            LOG.debug("Process instance created: " + processInstance.getId());
        } catch (Exception e) {
        }

        int numOfHistoryPI = historyService.createHistoricProcessInstanceQuery().list().size();
        assertEquals(1, numOfHistoryPI);
        System.out.println(("Number of Process Instance history = " + numOfHistoryPI));

    }
}
