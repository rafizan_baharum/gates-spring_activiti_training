package com.canang.training.router;

import com.canang.training.dao.RegistrationDao;
import com.canang.training.model.Registration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author rafizan.baharum
 * @since 1/22/14
 */
@Component("router")
public class Router {

    @Autowired
    private RegistrationDao registrationDao;

    public String findReceiveCandidateGroups(Long registrationId) {
        Registration registration = registrationDao.findById(registrationId);
        return "GROUP KERANI " + registration.getCourseName();
    }

    public String findCallCandidateGroups(Long registrationId) {
        Registration registration = registrationDao.findById(registrationId);
        return "GROUP PEGAWAI " + registration.getCourseName();
    }
}
