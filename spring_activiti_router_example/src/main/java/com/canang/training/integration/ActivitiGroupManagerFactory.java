package com.canang.training.integration;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.GroupManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("activityGroupManagerFactory")
public class ActivitiGroupManagerFactory implements SessionFactory{

    @Autowired
    private ActivitiGroupManager groupManager;

    @Override
    public Class<?> getSessionType() {
        return GroupManager.class;
    }

    @Override
    public Session openSession() {
        return groupManager;
    }
}
