package com.canang.training.integration;

import com.canang.training.dao.GroupDao;
import com.canang.training.dao.UserDao;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.persistence.entity.IdentityInfoEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.persistence.entity.UserManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("activityUserManager")
public class ActivitiUserManager extends UserManager {

    private static final Logger log = Logger.getLogger(ActivitiUserManager.class);

    @Autowired
    private GroupDao groupDao;

    @Autowired
    private UserDao userDao;

    @Override
    public User createNewUser(String userId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void insertUser(User
                                   user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public UserEntity findUserById(String userId) {
        return new ActivitiUser(userDao.findByUsername(userId));
    }

    @Override
    public void deleteUser(String userId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Group> findGroupsByUser(String userId) {
        log.debug("username:" + userId);
        com.canang.training.model.User byUsername = userDao.findByUsername(userId);
        List<com.canang.training.model.Group> principalGroups = groupDao.findMemberships(byUsername);
        List<Group> groups = new ArrayList<Group>();
        for (com.canang.training.model.Group group : principalGroups) {
            log.debug("group:" + group);
            ActivitiGroup g = new ActivitiGroup(group);
            groups.add(g);
        }
        return groups;
    }

    @Override
    public UserQuery createNewUserQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public IdentityInfoEntity findUserInfoByUserIdAndKey(String userId, String key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> findUserInfoKeysByUserIdAndType(String userId, String type) {
        throw new UnsupportedOperationException();
    }
}
