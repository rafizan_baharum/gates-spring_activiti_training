package com.canang.training.integration;

import com.canang.training.model.User;
import org.activiti.engine.impl.persistence.entity.UserEntity;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
public class ActivitiUser extends UserEntity {

    private User user;

    public ActivitiUser(User user) {
        this.user = user;
    }

    @Override
    public String getId() {
        return user.getUsername();
    }

    @Override
    public void setId(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getFirstName() {
        return user.getUsername();
    }

    @Override
    public void setFirstName(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setLastName(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getLastName() {
        return user.getUsername();
    }

    @Override
    public void setEmail(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getEmail() {
        return user.getUsername();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public void setPassword(String s) {
        throw new UnsupportedOperationException();
    }
}
