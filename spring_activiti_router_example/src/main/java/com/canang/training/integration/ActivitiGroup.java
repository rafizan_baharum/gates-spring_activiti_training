package com.canang.training.integration;

import com.canang.training.model.Group;
import org.activiti.engine.impl.persistence.entity.GroupEntity;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
public class ActivitiGroup extends GroupEntity {

    private Group group;

    public ActivitiGroup(Group group) {
        this.group = group;
    }

    @Override
    public String getId() {
        return group.getName();
    }

    @Override
    public void setId(String id) {
    }

    @Override
    public String getName() {
        return group.getName();
    }

    @Override
    public void setName(String name) {
        group.setName(name);
    }

    @Override
    public String getType() {
        return "group";
    }

    @Override
    public void setType(String string) {
    }
}
