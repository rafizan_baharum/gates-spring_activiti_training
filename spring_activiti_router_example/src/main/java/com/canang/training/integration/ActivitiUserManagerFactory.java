package com.canang.training.integration;

import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("activityUserManagerFactory")
public class ActivitiUserManagerFactory implements SessionFactory {

    @Autowired
    private ActivitiUserManager userManager;

    @Override
    public Class<?> getSessionType() {
        return UserManager.class;
    }

    @Override
    public org.activiti.engine.impl.interceptor.Session openSession() {
        return userManager;

    }
}
