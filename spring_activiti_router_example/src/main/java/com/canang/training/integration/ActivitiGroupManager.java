package com.canang.training.integration;

import com.canang.training.dao.GroupDao;
import com.canang.training.dao.UserDao;
import com.canang.training.model.User;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.persistence.entity.GroupManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Component("activitiGroupManager")
public class ActivitiGroupManager extends GroupManager {

    private static final Logger log = Logger.getLogger(ActivitiGroupManager.class);

    @Autowired
    private GroupDao groupDao;

    @Autowired
    private UserDao userDao;


    @Override
    public Group createNewGroup(String groupId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void insertGroup(Group group) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteGroup(String groupId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public GroupQuery createNewGroupQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Group> findGroupsByUser(String userId) {
        User user = userDao.findByUsername(userId);
        List<Group> groups = new ArrayList<Group>();
        try {
            List<com.canang.training.model.Group> cgroups = groupDao.findMemberships(user);
            for (com.canang.training.model.Group cgroup : cgroups) {
                ActivitiGroup g = new ActivitiGroup(cgroup);
                groups.add(g);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return groups;
    }
}
