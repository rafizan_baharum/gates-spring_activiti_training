package com.canang.training.model;

import javax.persistence.*;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@Entity(name = "Group")
@Table(name = "GROUPX")
public class Group {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_GROUP")
    @SequenceGenerator(name = "SEQ_GROUP", sequenceName = "SEQ_GROUP", allocationSize = 1)
    private Long id;

    @Column(name = "NAME")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
