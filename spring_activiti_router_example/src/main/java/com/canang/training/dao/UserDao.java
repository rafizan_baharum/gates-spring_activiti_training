package com.canang.training.dao;

import com.canang.training.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author rafizan.baharum
 * @since 1/19/14
 */
@SuppressWarnings({"unchecked"})
@Repository("userDao")
public class UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public User findById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return (User) session.get(User.class, id);
    }

    public User findByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r from User r where r.username = :username");
        query.setString("username", username);
        return (User) query.uniqueResult();
    }

    public List<User> find() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r from User r");
        return (List<User>) query.list();
    }

    public void save(User User) {
        Session session = sessionFactory.getCurrentSession();
        session.save(User);
    }
}
