package com.canang.training;

import com.canang.training.config.DataSourceConfiguration;
import com.canang.training.config.WorkflowConfiguration;
import junit.framework.Assert;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : alif haikal razak
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataSourceConfiguration.class, WorkflowConfiguration.class})
@Transactional
public class ConfigTest {

    private static final Logger LOG = Logger.getLogger(ConfigTest.class);

    @Autowired
    private ProcessEngine processEngine;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RepositoryService repositoryService;

    @Test
    @Rollback(true)
    public void startProcess() throws Exception {
        Assert.assertEquals("Workflow Engine", processEngine.getName());
        Assert.assertNotNull("", runtimeService);
        Assert.assertNotNull("", taskService);
        Assert.assertNotNull("", repositoryService);
    }
}
